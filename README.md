Proyecto de 2º trimestre para la asignatura de Diseeño de Aplicaciones Web (DIW) por Alejandro Vañó Tomás.

Para ponerlo en marcha:

- npm install

Si queremos usar el transpilador de SCSS a CSS

- npm run sass

Es un proyecto de HTML y SCSS de una web estática portfolio que incluye los siguientes elementos:

- HTML5 (section, main, aside, em, article...)
- CSS3
- No DIVs
- Animaciones
- Transformaciones
- Flexbox
- Grid
- Metodologia BEM
- Responsive
- Sass
- Funciones SCSS
- Mixins SCSS
- Uso de bucles For en SCSS
- Elemento <picture>
- Variables SCSS
- Uso de selectores
- Todo documentado
- Separado por secciones
- Menu hamburguesa
- Uso del @import, @include, @content, @extend, @for, @if, @else
- Operador ternario SCSS
- Todas las imagenes han sido optimizadas (https://imagecompressor.com/es/)
- Favicon en diferentes navegadores, formato SVG incluido
- Plugin Wave para accesibilidad comprobado y solucionado todos sus errores
- Uso de etiquetas aria (aria-label, aria-hidden)
- Humans.txt
- Robots.txt
- Validación correcta (https://validator.w3.org/)
- SVG Animado ***OJO*** Funciona correctamente con Chrome, Con firefox no se ve correctamente
- Uso de prefijos (https://autoprefixer.github.io/)
- CSS3 Linear Gradient
- Media querys Print
- Usado extensión Ligthouse
    - Perfomance 99
    - Accesibility 100
    - Best Practices 93
    - SEO 90
- Uso del Clip-path
- Uso del mask
- Uso de tipografia, importada desde google fonts
- Uso de tipografia, descargada
- Form
- Validación HTML5
- Formulario accesible
- Diferentes tipos de inputs HTML5 (color, date, file)
- Uso de atributos nuevos de HTML5 (pattern, min, max, step)

Material usado:
- [Animación logo](http://www.develapps.com/es/noticias/svg-como-animar-un-logo)
- [Proyecto base](https://www.youtube.com/watch?v=HguAyYnWBuU) 
- [Validación HTML5](https://lenguajehtml.com/p/html/formularios/validaciones-html5)
- [Accesibilidad radio buttons](https://www.w3.org/TR/2016/WD-wai-aria-practices-1.1-20161214/examples/radio/radio-1/radio-1.html)
- [Patterns colors](http://html5pattern.com/Colors)
- [Media print](https://www.smashingmagazine.com/2015/01/designing-for-print-with-css/)
- [Operador ternario SCSS](https://gist.github.com/MoOx/4266210)
- [Favicon](https://www.favicon-generator.org/)
- [W3S](https://www.w3schools.com/)